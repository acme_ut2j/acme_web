<div class="footer__container">
      <footer class="footer">
        <ul class="list_footer">
          <li>AIDE</li>
          <hr>
          <li><a href="#">Contactez nous</a></li>
          <li><a href="#">FAQ</a></li>
        </ul>
  
        <ul class="list_footer">
          <li>CONTACT</li>
          <hr>
          <li><a href="mailto:#">business-acme@acme.fr</a><li>
          <li>Phone number : 0XXXXXXXXX</li>
          <li>FAX : (XXX) XXX-XXXX</li>
        </ul>

        <img class="logo__img" src="<?=linkToRelPath("Assets/logo.png")?>" alt="logo">
        
        <div class="social__links">
          <!-- Add font awesome icons -->
          <a href="#" class="fa fa-facebook"></a>
          <a href="#" class="fa fa-twitter"></a>
        </div>

      </footer>
</div>
