<?php
    require '../db_commands/db_connec.php';
?>

<?php
    if(isset($_GET['id'])) {
        $req = $bdd->prepare("SELECT * FROM Fonctionnalites
                    where id = ?");
        $req->execute(array($_GET['id']));
        $fonctinfo=$req->fetch();
        $idfonct = $_GET['id'];
     } else {
            header('Location: gerer_fonctionnalite.php');
            }
    
    $message = "Voulez-vous vraiment supprimer ".$fonctinfo['titre'];


    if(isset($_POST['confirmer'])) {
        $bdd->query("DELETE FROM fonctionnalites
                     where id =".$_GET['id']);
        $message = "Suppresion réussie ! </br></br> <a class='linkTOgc' href='gerer_fonctionnalite.php'>Cliquez ici pour revenir sur la page de gestion</a>";

    }
    
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/supp_fonctionnalite.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/callBack.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>ACME - Accueil Administrateur - Gérer une Fonctionnalité</title>
  </head>
  <body>
  
    <?php
      require 'header.php';
    ?>

    <div class="container">
      <aside class="sidebar">
        <h1>Espace administrateur</h1>
        <hr>
        <nav>
          <ul>
            <li><a href="ajout_client.php">Ajouter un compte client</a></li>
            <li><a href="gerer_client.php">Gérer un compte client</a></li>
            <li><a href="ajout_fonctionnalite.php">Ajouter une fonctionnalité</a></li>
            <li><a href="gerer_fonctionnalite.php">Gérer une fonctionnalité</a></li>
          </ul>
        </nav>
      </aside>

    
      <section class="sup_fonc__sec">
        <h1 class="heading"><?=$message?></h1>
        <form action="" method="POST">
        <?php if(isset($_POST['confirmer'])): ?>
        <?php else: ?>
        <input class="btn" type="submit" name="confirmer" value="Confirmer la suppression"/>
        <?php endif; ?>
        </form>
      </section>
    
  </div>
  </body>
</html>