<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../CSS/forbidden.css">
    <title>Désolé</title>
</head>
<body>
    <div class="scene">
    <div class="overlay"></div>
    <div class="overlay"></div>
    <div class="overlay"></div>
    <div class="overlay"></div>
    <span class="bg-403">403</span>
    <div class="text">
        <span class="hero-text"></span>
        <span class="msg">On ne peut pas <span>vous</span> donner l'accès.</span>
        <span class="support">
        <span>inattendu?</span>
         <a href="login.php">Connectez-vous</a>
    </span>
  </div>
  <div class="lock"></div>
</div>
</body>
</html>