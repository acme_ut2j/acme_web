<?php
    require '../db_commands/db_connec.php';
    use App\Auth;
    use App\User;
    include '../Classes/Auth.php';
    include '../Classes/User.php';

    $auth = new Auth($bdd);
    $user = $auth->user();

    if( !$user ) {
      header("Location: forbidden.php");
      exit();
    }
?>

<?php
  function password_generate($chars) {
    $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($data), 0, $chars);
  }
?>

<?php
    $message = "";

    if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['mail'])){
      $nom = htmlspecialchars($_POST['nom']);
      $prenom = htmlspecialchars($_POST['prenom']);
      $mail = htmlspecialchars($_POST['mail']);

      $reqmail = $bdd->prepare("SELECT  * FROM clients where mail = ?");
      $reqmail->execute(array($mail));
      $mailexist = $reqmail->rowCount();

      if($mailexist == 0){
        $hash=password_hash(password_generate(10), PASSWORD_DEFAULT);

        $sql = "INSERT INTO clients (nom, prenom, mail, mdp) VALUES (?, ?, ?, ?)";

        $result = $bdd->prepare($sql);
        $result->execute(array($nom, $prenom, $mail, $hash ));

        $message = "Le client a été ajouté";
      }else{
        $message = "Erreur : le client exite déjà";
      }

      
    }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/ajout_client.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>ACME - Accueil Administrateur - Ajouter un client</title>
  </head>
  <body>
  
    <?php
        require 'header.php';
      ?>
    <div class="container">
      <aside class="sidebar">
        <h1>Espace administrateur</h1>
        <hr>
        <nav>
          <ul>
            <li><a href="ajout_client.php">Ajouter un compte client</a></li>
            <li><a href="gerer_client.php">Gérer un compte client</a></li>
            <li><a href="ajout_fonctionnalite.php">Ajouter une fonctionnalité</a></li>
            <li><a href="gerer_fonctionnalite.php">Gérer une fonctionnalité</a></li>
          </ul>
        </nav>
      </aside>

      <section class="ajouter_client__sec">
        <div class="form_container">
          <h1 class="heading">Ajout d'un compte client</h1>

            <?php 
            echo "<h4 class='result_msg'>".$message."</h4>";
            ?>

          <form class="form_ajout" action="" method="POST">
              <input type="text" name="nom" placeholder="Nom Client" Required><br>
              <input type="text" name="prenom" placeholder="Prénom Client" Required><br>
              <input type="email" name="mail" placeholder="Mail Client" Required><br>
              <button name="Valider" class="btn">Valider</button>
          </form>
        </div>   
      </section>

    </div>

  </body>
</html>

