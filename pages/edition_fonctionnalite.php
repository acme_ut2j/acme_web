<?php
    require '../db_commands/db_connec.php';

    if(isset($_GET['id'])) {
      $req4 = $bdd->prepare("SELECT * FROM Fonctionnalites
                  where id = ?");
      $req4->execute(array($_GET['id']));
      $fonctinfo=$req4->fetch();
      $idfonct = $_GET['id'];
   } else {
          header('Location: gerer_fonctionnalite.php');
          }
?>

<?php
    if(!empty($_POST['numSaison']) AND !empty($_POST['description']) AND !empty($_POST['titre'])){
        $titre = $_POST['titre'];
        $descrip = $_POST['description'];
        $saison = $_POST['numSaison'];

        $sql = "UPDATE fonctionnalites SET titre = ?,
                                           description = ?,
                                           saison = ?
                                        WHERE id = ".$idfonct;

        $result = $bdd->prepare($sql);
        $result->execute(array($titre, $descrip, $saison ));
        header('Location: gerer_fonctionnalite.php');
    }

    $numSaison = $bdd->query("SELECT num FROM saisons");
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/edition_fonctionnalite.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>ACME - Accueil Administrateur - Editer une Fonctionnalité</title>
  </head>
  <body>
  
    <?php
      require 'header.php';
    ?>

    <div class="container">
      <aside class="sidebar">
        <h1>Espace administrateur</h1>
        <hr>
        <nav>
          <ul>
            <li><a href="ajout_client.php">Ajouter un compte client</a></li>
            <li><a href="gerer_client.php">Gérer un compte client</a></li>
            <li><a href="ajout_fonctionnalite.php">Ajouter une fonctionnalité</a></li>
            <li><a href="gerer_fonctionnalite.php">Gérer une fonctionnalité</a></li>
          </ul>
        </nav>
      </aside>

    
      <section class="edit_fonc__sec">
        <div class="form_container">  
          <h1 class="heading">Formulaire d'édition de fonctionnalité</h1>
          <form class="form_edit_fonc" action="" method="POST">
              <input type="text" name="titre" placeholder="Titre de la Fonctionnalité" value='<?php echo($fonctinfo['titre']);?>'/><br>
              <label for="description">Veuillez ajoutez une description</label><br>
              <textarea id="desc" name="description" rows="5" cols="30" placeholder="Ici votre description"><?php echo($fonctinfo['description']);?></textarea><br>
              <div class="box">
                <select name="saison_num">
                <?php
                    foreach($numSaison as $num){
                    echo("<option value=".$num['num'].">Saison ".$num['num']."</option>");   
                    }
                ?>
                </select>
              </div>
              <button class="btn" name="Valider">Valider</button>
          </form>
        </div>
      </section>
    </div>
    
  </body>
</html>