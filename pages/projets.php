<?php 
  require '../db_commands/db_connec.php';
  require '../db_commands/db_saison_info.php';
  require '../db_commands/project_query.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/projets.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>SAISON <?php echo( $saisoninfo['num'] ); ?></title>
    
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  </head>
  <body>
    <?php
      $page = 'projets.php';
      require '../pages/header.php';
    ?>

    <div class="container">
      <section class="Projects_clients__data">
        <h1 class="Saison__title">Projets saison <?php echo ($saisoninfo['num']);?> </h1>
        <div class="projet_container">
          <?php 
            $precedent = -1;  // initialisation de la variable id du projet
            while ($row = $req_p1->fetch()) {
              if ( !($row['projet'] == $precedent) ) {
                // Display of project information
                echo ( "<div class='projet_info'>" );
                echo ( "<div class='item__project_number'>Projet " . htmlspecialchars($row['projet']) . "</div>" . "\n" );
                echo ( "<div class='item_projet'>" . "\n" );
                    echo ( "<div class='item__project_body'>" . "\n" );
                      echo ( "<div class='item__project_title'>" . htmlspecialchars($row['titre']) . "</div>" . "\n" );
                      echo ( "<div class='item__project_description'>" . htmlspecialchars($row['description']) . "</div>" . "\n" );
                    echo ( "</div>" . "\n" );
                    $req_p2 = $bdd->prepare($stat);
                    $req_p2->execute(array($_GET['num']));
                    echo ( "<div class='clients_sec'>" );
                    echo ( "<div class='clients__title'>Liste des clients</div>" . "\n" );
                    while ($row1 = $req_p2->fetch()) {
                      if ( $row1['projet'] == $row['projet'] ) {
                        echo ( "<div class='item_clients'>" . "\n" );
                          // Display of project CLIENTS
                          echo ( "<div class='item__client_body'>" . "\n" );
                            echo ( "<hr>" );
                            echo ( "<div class='item__client_name'>" . htmlspecialchars($row1['nom']) . "</div>" . "\n" );
                            echo ( "<div class='item__client_prenom'>" . htmlspecialchars($row1['prenom']) . "</div>" . "\n" );
                          echo ( "</div>" . "\n" );
                        echo ( "</div>" . "\n" );
                      }
                    }
                    echo ( "</div>" . "\n" ); 
                echo ( "</div>" . "\n" ); 
                echo ( "</div>" . "\n" );   
              }
              $precedent = $row['projet'];
            }
          ?>
        </div>
      </section>
    </div>
      
    <?php 
      require '../pages/footer.php';
    ?>

  </body>
</html>
