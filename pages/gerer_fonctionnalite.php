<?php
    require '../db_commands/db_connec.php';
    use App\Auth;
    use App\User;
    include '../Classes/Auth.php';
    include '../Classes/User.php';

    $auth = new Auth($bdd);
    $user = $auth->user();

    if( !$user ) {
      header("Location: forbidden.php");
      exit();
    }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/gerer_fonctionnalite.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>ACME - Accueil Administrateur - Gérer une Fonctionnalité</title>
  </head>
  <body>
  
  <?php
      require 'header.php';
    ?>

    <div class="container">
      <aside class="sidebar">
        <h1>Espace administrateur</h1>
        <hr>
        <nav>
          <ul>
            <li><a href="ajout_client.php">Ajouter un compte client</a></li>
            <li><a href="gerer_client.php">Gérer un compte client</a></li>
            <li><a href="ajout_fonctionnalite.php">Ajouter une fonctionnalité</a></li>
            <li><a href="gerer_fonctionnalite.php">Gérer une fonctionnalité</a></li>
          </ul>
        </nav>
      </aside>

      <?php
        $fonctionnalite = $bdd->query("SELECT * FROM Fonctionnalites")
      ?>
      
      <section class="gerer_fonc__sec">
        <div class="container__fonc">
          <h1 class="heading">Gérer une fonctionnalité</h1>
          <p class="desc">Liste des fonctionnalités</p>
              <?php
                echo( "<table>" );
                  echo( "<tr>" );
                    echo( "<th>Numéro</th>" );
                    echo( "<th>Titre</th>" );
                    echo( "<th>Description</th>" );
                    echo( "<th>Editer</th>" );
                    echo( "<th>Supprimer</th>" );
                  echo( "</tr>" );
                  foreach($fonctionnalite as $f) {
                    echo( "<tr>" );
                      echo( "<td>".$f['id']."</td>" );
                      echo( "<td>".$f['titre']."</td>" );
                      echo( "<td>".$f['description']."</td>" );
                      echo( "<td><a href='edition_fonctionnalite.php?id=".$f['id']."'><i class='fas fa-user-edit'></i></a></td>" );
                      echo( "<td><a href='supp_fonctionnalite.php?id=".$f['id']."'><i class='fas fa-user-times'></i></a></td>" );
                    echo( "</tr>" );
                  }
                echo( "</table>" );
              ?>
        </div>
      </section>
    </div>

  </body>
</html>