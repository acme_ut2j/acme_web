<?php
    function isIndexFile(string $p)
    {// Indique si $p représente un chemin vers un fichier index Apache.
     // Attention: ne vérifie pas l'extension de fichier.
        return pathinfo($p)['filename'] === 'index';
    }

    /* IMPORTANT: Le comportement des fonctions ci-dessous repose
     * sur la structure actuelle de notre répo (selon laquelle toutes
     * les pages Web, sauf la page d'accueil, sont stockées dans
     * un dossier nommé "pages").
     *
     * De plus, on assume que cette page d'accueil est stocké dans le
     * fichier "index.php" et qu'il n'y a aucun fichier nommé
     * "index.php" ou "index.html" dans le dossier "pages".
     */

    /// Le nom du dossier où se situe les pages du site (sauf l'index).
    define("PAGES", "pages");

    /// Constante indiquant si le fichier depuis lequel ce fichier est
    /// appelé est à la racine du serveur.
    define("AT_ROOT_DIR", isIndexFile(get_included_files()[0]));

    function linkToRelPath(string $filePath)
    {/* Crée un lien vers le fichier à l'endroit indiqué, utilisable
      * depuis n'importe quelle page du site Web. Le chemin vers le
      * fichier doit être relatif à la racine de la répo (voir 
      * AT_ROOT_DIR).
      */
        return AT_ROOT_DIR ? $filePath : "../$filePath";
    }

    function href(string $page)
    {/* Crée un lien vers la $page afin d'être utilisé dans l'attribut
      * href d'une balise <a> dans notre site, suivant la structure où
      * tous les fichiers (sauf la page d'accueil) se trouve dans le
      * répertoire "pages".
      */
        return AT_ROOT_DIR ? PAGES."/$page" : $page;
    }

    $homePage = linkToRelPath('index.php');
    $saisonNums = array_map(function($row) { return $row['num']; }, $num->fetchAll());
?>

<?php 
    if( session_status() === PHP_SESSION_NONE) {
        session_start();
    }
?>

<header class="topbar">
<h1 class="logo">
    <a href='<?=$homePage?>'>A<span class="logo__custom">C</span>ME</a>
</h1>
<nav>
    <ul>
        <li class="<?php if( $page == 'index.php' ) { echo 'active'; } ?>"><a href="<?=$homePage?>">Accueil</a></li>

        <li class ="deroulant <?php if( $page == 'saisons.php' ) { echo 'active'; } ?>"><a href="#">Saisons</a>
            <ul class="sous">
                <?php
                    $saisonsLink = href("saisons.php");
                    foreach ($saisonNums as $n) {
                        echo "<li><a href=\"$saisonsLink?num=$n&id=0\">Saison $n</a></li>";
                    }
                ?>
            </ul>
        </li>

        <li class ="deroulant <?php if( $page == 'projets.php' ) { echo 'active'; } ?>"><a href="#">Projets</a>
            <ul class="sous">
                <?php
                    $projetsLink = href("projets.php");
                    foreach ($saisonNums as $n) {
                        echo "<li><a href='$projetsLink?num=$n'>Projets - Saison $n</a></li>";
                    }
                ?>
            </ul>
        </li>
    </ul>
</nav>
    <?php if( empty($_SESSION) ): ?>
        <div class="login__btn">
        <a href='<?=href("login.php")?>'>Se connecter</a>
        </div>
    <?php endif; ?>
    <?php if( !empty($_SESSION) && $_SESSION['role'] == 'admin' ): ?>
        <nav class="log__nav">
            <ul>
                <li class ="deroulant"><a href="#">Compte ADMIN</a>
                    <ul class="sous">
                        <li><a href='<?=href("accueil_admin.php")?>'>Espace Gestion</li>
                        <li><a href='<?=href("../Classes/logout.php")?>'>Se déconnecter</li>
                    </ul>
                </li>
            </ul>
        </nav>
    <?php endif; ?>
    <?php if ( !empty($_SESSION) && $_SESSION['role'] == 'client' ): ?>
        <nav class="log__nav">
            <ul>
                <li class ="deroulant"><a href="#">Compte client</a>
                    <ul class="sous">
                        <li><a href='<?=href("contact.php")?>'>Contacter l'admin</a></li>
                        <li><a href='<?=href("../Classes/logout.php")?>'>Se déconnecter</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    <?php endif; ?>
</header>
