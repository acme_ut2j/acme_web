<?php
    require '../db_commands/db_connec.php';

    if(isset($_GET['num']))
    {
        $req = $bdd->prepare("SELECT * FROM participations p
                join membres on membres.id = p.membre
                join saisons on saisons.id = p.saison
                where saisons.num = ?");
        $req->execute(array($_GET['num']));
        $saisoninfo = $req->fetch();
    }


    if(isset($_GET['id']))
    {
        $req2 = $bdd->prepare(
          "SELECT titre, description as d, id FROM fonctionnalites 
            where saison = ".$saisoninfo['num']." AND id=?");
        $req2->execute(array($_GET['id']));
        $foncinfo = $req2->fetch();
    }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/saisons.css">
    <link rel="stylesheet" href="../CSS/footer.css">

    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <title>SAISON <?php echo ($saisoninfo['num']);?></title>
  </head>
  <body>
      
    <?php
      $page = 'saisons.php';
      require '../pages/header.php';
    ?>

    <div class="container">
      <section class="saisons__presentation">
        <div class="container__title">
          <h1 class="Saison__title">Saison <?php echo ($saisoninfo['num']);?>
            <br> 
              <?php echo("(".$saisoninfo['date_debut']." / ".$saisoninfo['date_fin'].")")?>
          </h1>
        </div>
        <div class="container__fonctionnalite">
            <section class="liste__fonctionnalite">
              <h2>Les fonctionnalités</h2>
              <?php
                $listeFonc=$bdd->query("SELECT titre, id FROM fonctionnalites WHERE saison = ".$saisoninfo['num']."");
                echo"<ul>";
                foreach($listeFonc as $f)
                {
                  echo"<li><a href='saisons.php?num=".$saisoninfo['num']."&id=".$f['id']."' >".$f['titre']."</a></li>";
                }
                echo"</ul>";
              ?>
            </section>

            <article class="description__saison">
              <?php 
                $descFonc = $bdd->query("SELECT titre, description as d, id FROM fonctionnalites where saison = ".$saisoninfo['num']." GROUP BY saison");
              ?>

              <p class="saisons_fonc__description" id="description_fonc">
                <?php
                  if($_GET['id'] == 0){
                    echo ('Bienvenue ! (description de la saison)');
                  }else{
                    echo ($foncinfo['d']);
                  }
                ?>
              </p>
            </article>
        </div>
      </section>  
        
      <section class="section_membres">
          <div class="container__membres">
            <h1 class="heading">Membres de la saison</h1>
              <div class="card_wrapper">
                  <?php
                    $membres=$bdd->query(
                      "SELECT nom, prenom, bio, photo FROM participations join membres on membres.id = participations.membre join saisons on saisons.id = participations.saison where saison =".$saisoninfo['num']);

                    foreach($membres as $m)
                    {
                        echo "<div class='card'>";
                        
                          //echo"<img src='".$m['photo']."' />";
                          echo "<img src='../Assets/colleagues-giving-fist-bump.jpg' alt='arriere plan image' class='card_img'/>";
                          echo "<img src='../Assets/".$m['photo']."' alt='image membre' class='profile_img'/>";
                          echo "<h1>".$m['nom']." ".$m['prenom']."</h1>";
                          echo "<a href='#' class='btn'>Contact</a>";

                          echo "<ul class='social_media'>";
                            echo " <li><a href='#'><i class='fab fa-linkedin'></i></a></li> ";
                            echo " <li><a href='#'><i class='fab fa-facebook-square'></i></a></li> ";
                            echo " <li><a href='#'><i class='fab fa-twitter-square'></i></a></li> ";
                            echo " <li><a href='#'><i class='fab fa-instagram-square'></i></a></li> ";
                          echo "</ul>";
                        echo "</div>";
                    }
                  ?>
              </div>          
          </div>  
      </section>
    </div>   
  </body>
</html>