<?php
    require '../db_commands/db_connec.php';

    if(isset($_GET['id'])) {
      $req3 = $bdd->prepare("SELECT * FROM Clients
                  where id = ?");
      $req3->execute(array($_GET['id']));
      $clientinfo=$req3->fetch();
      $idclient = $_GET['id'];
    } else {
        header('Location: gerer_client.php');
        }
?>

<?php
    if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['mail'])){
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $mail = $_POST['mail'];

        $sql = "UPDATE clients SET nom = ?,
                                   prenom = ?,
                                   mail = ?
                                   WHERE id = ".$idclient;

        $result = $bdd->prepare($sql);
        $result->execute(array($nom, $prenom, $mail ));
        header('Location: gerer_client.php');
    }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/edition_client.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>ACME - Accueil Administrateur - Editer un Profil Client</title>
  </head>
  <body>
  
    <?php 
      require 'header.php';
    ?>

    <div class="container">
      <aside class="sidebar">
        <h1>Espace administrateur</h1>
        <hr>
        <nav>
          <ul>
            <li><a href="ajout_client.php">Ajouter un compte client</a></li>
            <li><a href="gerer_client.php">Gérer un compte client</a></li>
            <li><a href="ajout_fonctionnalite.php">Ajouter une fonctionnalité</a></li>
            <li><a href="gerer_fonctionnalite.php">Gérer une fonctionnalité</a></li>
          </ul>
        </nav>
      </aside>

      <section class="edit_client__sec">
        <div class="form_container">
          <h1 class="heading">Formulaire d'édition de profil</h1>

        <form class="form_edit_client" action="" method="POST">
            <input type="text" name="nom" placeholer="Nom du client" value='<?php echo($clientinfo['nom']);?>'>
            <input type="text" name="prenom" placeholder="Prénom du client" value='<?php echo($clientinfo['prenom']);?>'>
            <input type="text" name="mail" placeholder="adresse mail" value='<?php echo($clientinfo['mail']);?>'>
            <button name="Modifier" class="btn">Modifier</button>
        </form>

      </section>
    </div>
  </body>
</html>


