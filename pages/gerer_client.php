<?php
    require '../db_commands/db_connec.php';
    use App\Auth;
    use App\User;
    include '../Classes/Auth.php';
    include '../Classes/User.php';

    $auth = new Auth($bdd);
    $user = $auth->user();

    if( !$user ) {
      header("Location: forbidden.php");
      exit();
    }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/gerer_client.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>ACME - Accueil Administrateur</title>
  </head>
  <body>
  
    <?php
      require 'header.php';
    ?>

    <div class="container">
      <aside class="sidebar">
        <h1>Espace administrateur</h1>
        <hr>
        <nav>
          <ul>
            <li><a href="ajout_client.php">Ajouter un compte client</a></li>
            <li><a href="gerer_client.php">Gérer un compte client</a></li>
            <li><a href="ajout_fonctionnalite.php">Ajouter une fonctionnalité</a></li>
            <li><a href="gerer_fonctionnalite.php">Gérer une fonctionnalité</a></li>
          </ul>
        </nav>
      </aside>

      <?php
        $client = $bdd->query("SELECT * FROM Clients")
      ?>

      <section class="gerer_client__sec">
        <div class="container__client">
          <h1 class="heading">Gérer un compte client</h1>
          <p class="desc">Liste clients</p>
              <?php
                echo( "<table>" );
                  echo( "<tr>" );
                    echo( "<th>Numéro</th>" );
                    echo( "<th>Nom</th>" );
                    echo( "<th>Prénom</th>" );
                    echo( "<th>Editer</th>" );
                    echo( "<th>Supprimer</th>" );
                  echo( "</tr>" );
                  foreach($client as $c) {
                    echo( "<tr>" );
                      echo( "<td>".$c['id']."</td>" );
                      echo( "<td>".$c['nom']."</td>" );
                      echo( "<td>".$c['prenom']."</td>" );
                      echo( "<td><a href='edition_client.php?id=".$c['id']."'><i class='fas fa-user-edit'></i></a></td>" );
                      echo( "<td><a href='supp_client.php?id=".$c['id']."'><i class='fas fa-user-times'></i></a></td>" );
                    echo( "</tr>" );
                  }
                echo( "</table>" );
              ?>
        </div>
        
      </section>
    </div>

  </body>
</html>