<?php
    use App\Auth;
    use App\User;
    include '../Classes/Auth.php';
    include '../Classes/User.php';
    require '../db_commands/db_connec.php';

    $erreur = false;
    session_start();

    if( !empty( $_POST) ) {
        $auth = new Auth($bdd);
        $user = $auth->login($_POST['username'], $_POST['password']);

        if( !$user )  $erreur = true;
    }

    if( session_status() === PHP_SESSION_ACTIVE) {
        if( !empty($_SESSION) && $_SESSION['role'] == 'admin') {
            header("Location: accueil_admin.php");
            exit();
        }
        
        if( !empty($_SESSION) && $_SESSION['role'] == 'client') {
            header("Location: contact.php");
            exit();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/login.css">
    <title>Authentication</title>
</head>
<body>
    <?php
      require 'header.php';
    ?>

    <div class="container">
        <form class="login-form" action="" method="POST">
                <div class="login-form__logo-container">
                    <img class="login-form__logo" src="../Assets/logo_transparent.png" alt="Logo">
                </div>
                <div class="login-form__content">
                    <div class="login-form__header">Connectez-vous à votre compte</div>
                    <?php if ($erreur): ?>
                    <p>Identifiant ou mot de passe incorrects</p>
                    <?php endif ?>
                    <input class="login-form__input" type="email" name="username" placeholder="Adresse e-mail" required>
                    <input class="login-form__input" type="password" name="password" placeholder="Mot de passe" required>
                    <button class="login-form__button" type="submit">Se connecter</button>
                    <div class="login-form__links">
                        <a class="login-form__link" href="./login.php">Mot de passe oublié?</a>
                    </div>
                </div>
        </form>
    </div>
</body>
</html>
