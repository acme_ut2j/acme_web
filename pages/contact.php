<?php
    require '../db_commands/db_connec.php';
?>

<?php 
    use App\Auth;
    use App\User;
    include '../Classes/Auth.php';
    include '../Classes/User.php';
    require '../db_commands/db_connec.php';

    $auth = new Auth($bdd);
    $user = $auth->user();
?>

<?php 
    $msg_success = false;
    if ( isset($_POST['email']) && $_POST['email'] != '' ) {
        if( filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
            $userName = $_POST['name'];
            $userEmail = $_POST['email'];
            $msgSubject = $_POST['subject'];
            $msg = $_POST['message'];
        
            $SendTo = "";   // here should be our business email
            $body = "";
        
            $body .=  "From: ".$userName. "\r\n";
            $body .=  "Email: ".$userEmail. "\r\n";
            $body .=  "Message: ".$msg. "\r\n";

            mail($SendTo, $msgSubject, $body);
            
            $msg_success = true;
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">  
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="../Assets/favicon.png">
        <link rel="stylesheet" href="contact.css" media="all">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>   
        <link rel="stylesheet" href="../CSS/menu.css"> 
        <link rel="stylesheet" href="../CSS/contact.css"> 
        <title>ACME - Contact</title>  
        <script src="contact.js" async defer></script>
    </head>
    <body>
        
        <?php
            require 'header.php';
        ?>

        <?php 
            if ($msg_success):
        ?>
            <div class="succes_msg">
                <h3>Votre e-mail a bien été envoyé, nous vous remercions de nous avoir contacter.</h3>
            </div>
        <?php 
            else:
        ?>

        <div class="main">
        <div class="container">
            <div class="acme">
            <h1 class="main_title">Contacter - ACME</h1>
            <img class="logo_acme" src="../Assets/logo_transparent.png" alt="logo ACME">
            </div>
            <form action="contact.php" method="POST" class="form">
                <div class="form-group">
                    <label for="name" class="form-label">Votre nom</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="<?= $user->nom . " - ". $user->prenom ?>" tabindex="1" required>
                </div>
                <div class="form-group">
                    <label for="email" class="form-label">Votre Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="<?= $user->mail ?>" tabindex="2" required>
                </div>
                <div class="form-group">
                    <label for="subject" class="form-label">Sujet</label>
                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Sujet du message" tabindex="3" required>
                </div>
                <div class="form-group">
                    <label for="message" class="form-label">Message</label>
                    <textarea class="form-control" rows="5" cols="50" id="message" name="message" placeholder="Enter Message..." tabindex="4" required></textarea>
                </div>
                <div>
                    <button type="submit" class="btn">Send Message!</button>
                </div>
            </form>
        </div>
        </div>

        <?php 
            endif;
        ?>
    </body>
</html>