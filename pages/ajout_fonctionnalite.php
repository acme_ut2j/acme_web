<?php
    require '../db_commands/db_connec.php';
    use App\Auth;
    use App\User;
    include '../Classes/Auth.php';
    include '../Classes/User.php';

    $auth = new Auth($bdd);
    $user = $auth->user();

    if( !$user ) {
      header("Location: forbidden.php");
      exit();
    }
?>

<?php
    if(!empty($_POST['saison_num']) AND !empty($_POST['description']) AND !empty($_POST['titre'])){
      $titre = $_POST['titre'];
      $descrip = $_POST['description'];
      $saison = $_POST['saison_num'];

      $sql = "INSERT INTO fonctionnalites (titre, description, saison) VALUES (?, ?, ?)";

      $result = $bdd->prepare($sql);
      $result->execute(array($titre, $descrip, $saison ));

      header('Location: gerer_fonctionnalite.php');
    }          
    
    $numSaison = $bdd->query("SELECT num FROM saisons");
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/menu.css">
    <link rel="stylesheet" href="../CSS/ajout_fonctionnalite.css">
    <link rel="stylesheet" href="../CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>ACME - Accueil Administrateur - Ajouter une Fonctionnalité</title>
  </head>
  <body>
  
    <?php
      require 'header.php';
    ?>

    <div class="container">
      <aside class="sidebar">
        <h1>Espace administrateur</h1>
        <hr>
        <nav>
          <ul>
            <li><a href="ajout_client.php">Ajouter un compte client</a></li>
            <li><a href="gerer_client.php">Gérer un compte client</a></li>
            <li><a href="ajout_fonctionnalite.php">Ajouter une fonctionnalité</a></li>
            <li><a href="gerer_fonctionnalite.php">Gérer une fonctionnalité</a></li>
          </ul>
        </nav>
      </aside>

      <section class="ajouter_fonc__sec">
        <div class="form_container">
          <h1 class="heading">Ajout d'une fonctionnalité</h1>
          <form class="form_ajout_fonc" action="" method="POST">
              <input type="text" name="titre" placeholder="Titre Fonctionnalité" Required><br>
              <label for="description" class="fonctionnalite__desc">Veuillez ajouter une description</label><br>
              <textarea id="desc" name="description" rows="5" cols="30" placeholder="Ici votre description" Required></textarea><br>
              <div class="box">
                  <select name="saison_num">
                  <?php
                      foreach($numSaison as $num){
                      echo("<option value=".$num['num'].">Saison ".$num['num']."</option>");   
                      }
                  ?>
                  </select>
              </div>
              <button class="btn" name="Valider">Valider</button>
          </form>
        </div>
      </section>
    </div>
    
  </body>
</html>
