<?php
namespace App;

class Auth {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }
    
    public function user(): ?User
    {
        if( session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $id = $_SESSION['auth'] ?? null;
        if ( $id === null ) {
            return null;
        } 
        if( $_SESSION['role'] == 'admin' ) {
            $query = $this->pdo->prepare('SELECT * FROM admins where id = ?');
            $query->execute([$id]);
            $user = $query->fetchObject(User::class);

            return $user ?: null;
        }
        if( $_SESSION['role'] == 'client' ) {
            $query = $this->pdo->prepare('SELECT * FROM clients where id = ?');
            $query->execute([$id]);
            $user = $query->fetchObject(User::class);

            return $user ?: null;
        }
    }

    public function login(string $username, string $password): ?User
    {
        // On verifie dans la table des clients si l'user est un client:
        $query1 = $this->pdo->prepare('SELECT * FROM clients WHERE mail = :username');
        $query1->execute(['username' => $username]);
        $client = $query1->fetchObject(User::class);
        if( $client == false ) {
            // On verifie dans la table des admins pour savoir si l'user est un Admin:
            $query2 = $this->pdo->prepare('SELECT * FROM admins WHERE email = :username');
            $query2->execute(['username' => $username]);
            $admin = $query2->fetchObject(User::class);

            if( $admin == false ) {
                return null;
            }
            // on l'a trouvé c'est l'admin qui veut se connecter
            if( password_verify($password, $admin->mdp )) {
                if( session_status() === PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['auth'] = $admin->id; 
                $_SESSION['role'] = 'admin'; 
                return $admin;
            }
            return null;
        }
        if( password_verify($password, $client->mdp )) {
            if( session_status() === PHP_SESSION_NONE) {
                session_start();
            }
            $_SESSION['auth'] = $client->id; 
            $_SESSION['role'] = 'client'; 
            return $client;
        }
        return null;
    }
}
