-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2020 at 02:40 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acme_saison1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(2) NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdp` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `mdp`) VALUES
(1, 'lektati1@gmail.com', '$2y$10$8XAFSB7UwhxCRAcbm/9x8OOxld5LM9mRNlzbPtDQu3NIdSSb11SkW'),
(2, 'ludovic@gmail.com', '$2y$10$2Y8ykYs84gASW/JHIxZuCOwgDeT8cxQO/oP7Q1PIXmBSzdYbefHpG'),
(3, 'cyphax@gmail.com', '$2y$10$FC1ElXNgwQhENXjYZhRruOKkpnEDvMWRmO2N6GjfZie0/DaIRpxf6'),
(4, 'amine@gmail.com', '$2y$10$sHQZJ4IuSoHC6TBLumMiuO874CmO15AuVkf3DO8gG5ssJHJW4hcIC'),
(5, 'jason@gmail.com', '$2y$10$mhyFuJDFXE3xUslrMLX/0O2tE9JlHUXJXKgJajJ8W4USg86FhYOSe');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(2) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `nom`, `prenom`, `mail`, `mdp`) VALUES
(1, 'Université Toulouse', 'Jean Jaurès', 'Jean-Jaures@univ.com', '$2y$10$bYBoooA1t27Kvo98M/zDyu4gs0Af5b2hp.aPKkxohraiUwVC7GdnO'),
(2, 'Multimed Solutions', 'Agence de Communication', 'Multimed-solutions@tech.com', '$2y$10$.FvGPFTpSXNb6F78Wl7XEePrybjQdEqjs6hHP5FKmdSS4RD1KOlh2'),
(3, 'Acila développement', 'web & desktop', 'Acila-web@dev.fr', '$2y$10$DK6ICTttHj6G03cF0/wVaeBhbrwJYvZn7hExcFqATcHLyNbJEiA52'),
(4, 'Desmotta', 'Agence web', 'Desmota-agence@tech.fr', '$2y$10$yAvlHaOMIyxVBXig0yE4VuHdJx94gCbtDWQFd486P5QgHGIJzXdjC');

-- --------------------------------------------------------

--
-- Table structure for table `commandes`
--

CREATE TABLE `commandes` (
  `client` int(2) NOT NULL,
  `projet` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `commandes`
--

INSERT INTO `commandes` (`client`, `projet`) VALUES
(1, 1),
(1, 3),
(2, 2),
(4, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `compositions`
--

CREATE TABLE `compositions` (
  `projet` int(3) NOT NULL,
  `saison` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `compositions`
--

INSERT INTO `compositions` (`projet`, `saison`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fonctionnalites`
--

CREATE TABLE `fonctionnalites` (
  `id` int(3) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `saison` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fonctionnalites`
--

INSERT INTO `fonctionnalites` (`id`, `titre`, `description`, `saison`) VALUES
(1, 'Authentification', 'L\'authentification pour un système informatique est un processus permettant au système de s\'assurer de la légitimité de la demande d\'accès faite par une entité afin d\'autoriser l\'accès de cette entité à des ressources du système conformément au paramétrage du contrôle d\'accès.', 1),
(2, 'Options afficher mot de passe', 'Les cybercriminels utilisent souvent des méthodes simples pour aboutir à leurs fins. Dans un lieu public, ils peuvent simplement s\'asseoir derrière vous et noter votre mot de passe pendant que vous le tapez sur votre appareil. Les développeurs peuvent contribuer à combattre ce problème en fournissant une option capable d’afficher ou de masquer le mot de passe lorsqu\'un utilisateur l’écrit. Cette méthode peut servir de première ligne de défense contre les pirates informatiques.', 2),
(3, 'Barre de recherche, avec filtrage dynamique', 'Même avec des écrans grand format et des fonctions d\'autocorrection, taper sur un écran mobile reste inconfortable et les utilisateurs doivent régulièrement retaper et corriger leurs frappes. Ce qui, lorsqu’ils recherchent un produit, peut vite devenir frustrant!\r\n\r\nLe filtrage dynamique lors d\'une recherche est l\'une des fonctionnalités de l\'application mobile qui rendra votre application plus intuitive et lui donnera, pendant la saisie, un look moderne. La plupart des plateformes mobiles ont des options de filtrage de données intégrées. Donc, le filtrage dynamique devrait être facile à mettre en œuvre.', 2);

-- --------------------------------------------------------

--
-- Table structure for table `membres`
--

CREATE TABLE `membres` (
  `id` int(3) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `membres`
--

INSERT INTO `membres` (`id`, `nom`, `prenom`, `mail`, `bio`, `photo`) VALUES
(1, 'Lektati', 'Mahdi', 'mahdi@mail', 'La biographie de Mahdi', '../Assets/mahdi.jpg'),
(2, 'Chrosrova', 'Jason', 'jason@mail', 'La biographie de Jason', '../Assets/jason.jpg'),
(3, 'Badèche', 'Amine', 'amine@mail', 'La bio d\'Amine', '../Assets/amine.jpg'),
(4, 'Orion', 'Ludovic', 'ludovic@mail', 'La bio de Ludovic', '../Assets/ludovic.jpg'),
(5, 'Ouhammou', 'Cyphax', 'cyphax@mail', 'La bio de Cyphax', '../Assets/cyphax.jpg'),
(6, 'DiCaprio', 'Leonardo', 'membre1@saison2', 'bio membre1 saison2', '../Assets/leonardo_decaprio.jpg'),
(7, 'Brad', 'Pitt', 'membre2@saison2', 'bio membre2 saison2', '../Assets/Brad_pitt.jpg'),
(8, 'Amanda', 'Seyfried', 'membre3@saison2-3', 'bio membre3 saison2-3', '../Assets/Amanda_Seyfried.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `participations`
--

CREATE TABLE `participations` (
  `saison` int(2) NOT NULL,
  `membre` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `participations`
--

INSERT INTO `participations` (`saison`, `membre`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 6),
(2, 7),
(2, 8),
(3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `projets`
--

CREATE TABLE `projets` (
  `id` int(3) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projets`
--

INSERT INTO `projets` (`id`, `titre`, `description`) VALUES
(1, 'Réservation de salles de répétition', 'Si vous ne réusissez pas a trouver l\'application pour gérer vos réservations précisément comme vous le désirez, vous avez finalement trouvé le logiciel de réservation adéquat!'),
(2, 'Application de facturation', 'Avec l\'augmentation du nombre de personnes travaillant à domicile ou sur le terrain, la capacité de créer, de gérer et d\'ajuster des factures en déplacement est indispensable. C\'est pourquoi le lancement avec votre start-up de la création d\'une application de facturation est un choix idéal. Une application de facturation permettra aux utilisateurs de produire des factures pour les clients et de suivre les avis de retard pour les années à venir.'),
(3, 'Application pour la recherche d\'alternance', 'La recherche d\'alternance n\'était jamais une étape facile pour les étudiants et pourtant les applications de recherche d\'emploi ont constamment remplacé les rubriques classées des journaux et les entreprises les utilisent désormais largement comme principale source d\'embauche. Pour cela, on a envisagez de créer une application qui aide à mettre en relation les étudiants en recherche d\'alternance et les employeurs afin de les aider à trouver un emploi qui correspond le mieux à leurs compétences.'),
(4, 'Application de tutorat', 'À moins que le besoin d\'aller à l\'école ne prenne fin, les étudiants auront toujours besoin d\'aide pour comprendre ce qu\'ils ont appris en classe. Pour cela, on a envisagé de créer une application de tutorat à la demande est une bonne idée, qui aide les érudiants à obtenir un tutorat lorsqu\'ils en ont besoin.');

-- --------------------------------------------------------

--
-- Table structure for table `saisons`
--

CREATE TABLE `saisons` (
  `id` int(2) NOT NULL,
  `num` int(2) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `saisons`
--

INSERT INTO `saisons` (`id`, `num`, `date_debut`, `date_fin`) VALUES
(1, 1, '2020-10-12', '2021-02-19'),
(2, 2, '2021-10-11', '2022-02-18'),
(3, 3, '2022-10-10', '2023-02-17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING HASH,
  ADD UNIQUE KEY `mdp` (`mdp`) USING HASH;

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`client`,`projet`),
  ADD KEY `projet` (`projet`);

--
-- Indexes for table `compositions`
--
ALTER TABLE `compositions`
  ADD PRIMARY KEY (`projet`,`saison`),
  ADD KEY `compositions_ibfk_2` (`saison`);

--
-- Indexes for table `fonctionnalites`
--
ALTER TABLE `fonctionnalites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `saison` (`saison`);

--
-- Indexes for table `membres`
--
ALTER TABLE `membres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participations`
--
ALTER TABLE `participations`
  ADD PRIMARY KEY (`saison`,`membre`),
  ADD KEY `participations_ibfk_1` (`membre`);

--
-- Indexes for table `projets`
--
ALTER TABLE `projets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saisons`
--
ALTER TABLE `saisons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `fonctionnalites`
--
ALTER TABLE `fonctionnalites`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `membres`
--
ALTER TABLE `membres`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `projets`
--
ALTER TABLE `projets`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `saisons`
--
ALTER TABLE `saisons`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`client`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `commandes_ibfk_2` FOREIGN KEY (`projet`) REFERENCES `projets` (`id`);

--
-- Constraints for table `compositions`
--
ALTER TABLE `compositions`
  ADD CONSTRAINT `compositions_ibfk_1` FOREIGN KEY (`projet`) REFERENCES `projets` (`id`),
  ADD CONSTRAINT `compositions_ibfk_2` FOREIGN KEY (`saison`) REFERENCES `saisons` (`id`);

--
-- Constraints for table `fonctionnalites`
--
ALTER TABLE `fonctionnalites`
  ADD CONSTRAINT `fonctionnalites_ibfk_1` FOREIGN KEY (`saison`) REFERENCES `saisons` (`id`);

--
-- Constraints for table `participations`
--
ALTER TABLE `participations`
  ADD CONSTRAINT `participations_ibfk_1` FOREIGN KEY (`membre`) REFERENCES `membres` (`id`),
  ADD CONSTRAINT `participations_ibfk_2` FOREIGN KEY (`saison`) REFERENCES `saisons` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
