# Instructions
## Comment installer et configurer `black`

[`black` est un outil permettant de formatter automatiquement du code Python.
](https://black.readthedocs.io/en/stable/)

**Prérequis:** Python 3.6 ou plus récent, `pip`.

- Installer `black` via le [Python Package Index](https://pypi.org/project/black/):
  `pip install black` (`pip` peut parfois être nommé `pip3` sur votre machine).

### Configurer `black` avec Visual Studio Code
- Installer l'extension python (par Microsoft)
- Quitter puis relancer Visual Studio Code
- Chercher dans les paramètres `python.formatter`
- Donner à l'option `Provider` la valeur `black`
- Utiliser le raccourci clavier Shift+Alt+F sur le fichier à reformatter
- Si ça donne une erreur changer l'option `Black Path` vers `python -m black`

### Configurer `black` avec Vim
- Sans le plugin `psf/black`:
  - Pour formatter un fichier, exécutez la commande `:!black %`
    - **N.B.:** `black` ne peut pas formatter depuis stdin, donc on ne peut pas faire
      `:%!black`
  - Pour formatter un fichier à l'écriture, il serait peut-être utile d'ajouter ceci
    dans votre `ftplugin` pour Python:
    ```vimscript
    augroup format_python_on_write
    autocmd!
    autocmd BufWritePost *.py silent execute '!black ' . @% | redraw!
    augroup END
    ```
- Avec le plugin `psf/black`:
  [installer avec votre gestionnaire de plugin préféré.
  ](https://github.com/psf/black/blob/master/docs/editor_integration.md#Vim)

### Configurer `black` avec un autre éditeur de texte
[`black` maintient une liste de tutoriels permettant de l'intégrer à d'autres éditeurs
de texte.](https://github.com/psf/black/blob/master/docs/editor_integration.md)

## Comment installer Selenium IDE

Selenium IDE est une extension pour navigateur Web permettant de créer, modifier,
exporter et lancer des tests fonctionnels sur le site Web du projet. Cette extension est
disponible sur
[Firefox](https://addons.mozilla.org/en-US/firefox/addon/selenium-ide/) et sur
[le Chrome Web Store (pour Chrome, Chromium, Microsoft Edge, Vivaldi, Opera, Brave...)
](https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd).

## Comment installer et utiliser WampServer (Windows seulement)

WampServer est un environnement de développement permettant d'installer un serveur
local. Au sein de ce projet, nous utilisons surtout WampServer car il permet d'installer
une base de données MariaDB et l'interface Web phpMyAdmin pour interagir avec elle.

- Télécharger WampServer en cliquant sur le bouton "Download Latest Version" à
  [ce lien](https://sourceforge.net/projects/wampserver/files/)
- Lancer le fichier installé (probablement nommé `wampserver3.?.?_x64.exe`) et suivre
  les instructions
- Vous pouvez laisser les paramètres par défaut, qui étaient, à la version 3.2.3 de
  WampServer (vous verrez ce texte à l'écran "*Prêt à installer*"):
  ```
  Dossier de destination :
        c:\wamp64

  Type d'installation :
        Installation par défaut

  Composants sélectionnés :
        Wampmanager
        Apache 2.4.46
        PHP 5.6.40
        PHP 7.3.21
        PHP 7.4.9
        MariaDB
           MariaDB 10.4.13
        MySQL
           MySQL 5.7.31
        Applications
           PhpMyAdmin 5.0.2
           Adminer 4.7.7
           PhpSysInfo 3.3.2

  Dossier du menu Démarrer :
        Wampserver64
  ```
- Selon l'expérience de l'auteur de ce texte (Jason Chrosrova), le choix de l'éditeur de
  texte n'importe peu. Vous pouvez en choisir un quand même si vous le souhaitez.
- Un autre prompt permet de choisir l'éditeur de texte par défaut.
  Ce choix n'est pas important.
  - **N.B.:** le Vim installé avec git bash est dans `C:\Program Files\Git\usr\bin`
    (ou `C:\Programmes\Git\usr\bin` en français). **Attention: non testé!**
- **Lisez les notices qui vous sont montrées dans l'avant-dernière page d'installation**
  - À savoir les notices situées à `C:\wamp64\instructions_utilisation.pdf` ou `.rtf` et
    à `C:\wamp64\mariadb_mysql.txt`.
- Pour lancer WampServer, lancer l'application Wampserver64 à partir du menu démarrer.
- Après lancement des services, le logo de WampServer devrait s'afficher en rouge, puis
  en jaune, puis en vert sur la partie droite de la barre des tâches.
  C'est à partir de ce logo que vous pourrez interragir avec le serveur.
- À l'installation de Wamp, le nom d'utilsateur à utiliser pour se connecter sur
  phpMyAdmin est "root" et il n'y a pas de mot de passe (laisser le champ vide).
- Quand vous avez fini de travailler avec WampServer, vous pouvez fermer le serveur en
  faisant un clic droit sur le logo de Wamp (toujours dans la barre de tâches), puis en
  cliquant sur *"Fermer"*.

### Comment définir votre dossier de travail comme un VirtualHost?

Créer un VirtualHost c'est définir un serveur local qui vous permettra d'interpréter
vos fichiers php.

Il faut que notre VirtualHost soit le dossier lié à GitLab. Donc on définit ce
dossier comme un VirtualHost.

1. **clic-gauche** sur le **logo Wamp**
2. **Vos VirtualHosts** -> clic sur **gestion VirtualHost** (sinon vous pouvez y accéder
   directement avec ce lien **http://localhost/add_vhost.php**)
3. Dans la case du formulaire 'Nom du VirtualHost' -> entrer le nom de votre VH sans
   underscore (ça: <kbd>_</kbd>)
4. Dans la case du formulaire 'chemin' -> entrer le **chemin ABSOLU du dossier** en
   lien avec votre gitlab (sans oublier le / à la fin)
5. Entrer sur 'démarrer la création du VirtualHost'

Votre VH sera ensuite disponible à l'URL `localhost` dans **Vos VirtualHosts** en bas
de la page (besoin d'un redémarrage de Wamp).

**N.B.:** Vous devrez créer un VirtualHost par projet (ici le projet tutoré) pour éviter
les conflits. Si vous travaillez sur un autre projet ayant besoin d'un serveur local,
 vous devrez définir un autre VirtualHost.

## Comment installer et utiliser XAMPP

XAMPP est une application aux fonctionnalités similaires à celles de WampServer, ayant
l'avantage d'être multi-plateformes. Le tutoriel ci-dessous explique comment l'installer
sur un Mac.

### Installer depuis le site web d'Apache Friends

- Télécharger XAMPP via [le site Web d'Apache Friends](https://www.apachefriends.org/)
  (le développeur de XAMPP), ouvrez le fichier `.dmg`, puis glissez XAMPP vers le
  raccourci vers le dossier "Applications".
- Lancez XAMPP et entrez votre mot de passe administrateur.
- Pour lancer XAMPP, cliquez sur *"Start"*. Attendez quelques instants, le temps que le
  serveur se lance.
- Dans la section "Network", cliquez sur *"Enable"*.
- Au premier démarrage, cliquez sur *"Mount"* dans la section "Volume", puis créez un
  lien symbolique vers la racine du serveur Apache qui stockera le site ACME_web dans un
  dossier de votre choix en effectuant la commande:
  ```sh
  ln -s ~/.bitnami/stackman/machines/xampp/volumes/root/htdocs $DOSSIER_DE_VOTRE_CHOIX
  ```
  Cela vous donnera un accès plus simple au serveur.
- Intégrez la repo dans le serveur Apache:
  - Si vous n'avez pas déjà cloné la répo sur votre machine, profitez-en pour le faire
    dès maintenant dans le dossier créé à l'étape précédente.
  - Si vous avez déjà cloné la répo sur votre machine, déplacez-la dans le serveur
    Apache à l'aide de la commande `mv`.
- Vous pouvez lancer phpMyAdmin via
  [localhost:8080/phpmyadmin/](localhost:8080/phpmyadmin/)
- Quand vous avez fini de travailler avec XAMPP, vous pouvez fermer le serveur en
  appuyant sur *"Stop"* dans l'application XAMPP. Le serveur se fermera également à la
  fermeture de l'application (ce qui implique que vous devez laisser l'application
  ouverte lorsque vous travaillez avec XAMPP).

# Installer avec Homebrew

Cette installation nécessite évidemment que vous ayez Homebrew sur votre machine.

- `brew install --cask xampp`. Pendant l'installation, l'invite de commandes vous
  demandera d'entrer votre mot de passe administrateur.
- Pour lancer XAMPP avec l'application prévue à cet effet située à
  `/Applications/XAMPP/manager-osx`.
- Dans la section "Manage Servers" cliquez sur "Start All".
- Optionnellement, créez un lien symbolique vers la racine du serveur Apache qui
  stockera le site ACME_web dans un dossier de votre choix avec la commande:
  ```sh
  ln -s /Applications/XAMPP/xamppfiles/htdocs $DOSSIER_DE_VOTRE_CHOIX
  ```
- Intégrez la répo dans le serveur Apache en suivant [les instructions définies dans le
  tutoriel ci-dessus](#installer-depuis-le-site-web-d-apache-friends).
- Vous pouvez lancer phpMyAdmin via [localhost/phpmyadmin](localhost/phpmyadmin)
- Quand vous avez fini de travailler avec XAMPP, vous pouvez fermer le serveur en
  appuyant sur *"Stop All"* avec manager-osx (l'application située à
  `/Applications/XAMPP/manager-osx`).

## Comment importer la base de données `BDD/acme_saison1.sql` sur phpMyAdmin ?

**Prérequis:** pouvoir se connecter sur phpMyAdmin (si vous êtes sur Windows, voir
[la partie sur WampServer
](#comment-installer-et-utiliser-wampserver-windows-seulement))

- Si ce n'est pas fait, créer une base de données **exactement** nommée `acme_saison1`.
  - Cliquer sur "Nouvelle base de données"
  - Entrez `acme_saison1`.
  - Sur la liste déroulante à droite, choisir `utf8_unicode_ci` au lieu de
    `latin_swedish_ci`.
- Cliquer sur le bouton "Importer".
- Choisissez le fichier à importer, laissez les options par défaut et appuyez sur
  "Exécuter".
