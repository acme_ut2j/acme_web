<?php
  require 'db_commands/db_connec.php';
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="Assets/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="stylesheet" href="CSS/menu.css">
    <link rel="stylesheet" href="CSS/footer.css">
    <script src="https://kit.fontawesome.com/e830ebe669.js" crossorigin="anonymous"></script>
    <title>BIENVENUE SUR ACME</title>
  </head>
  <body>
      
    <?php 
      $page = 'index.php';
      require 'pages/header.php';
    ?>

    <div class="container">
      <section class="homepage__description">
        <h1 class="homepage__title">A Company that Makes Everything</h1>
        <p class="homepage__paragraphe">
            ACME Corporation est une société qui permet de faire de tout dans
             le développement:
             organisation d'évènements, gestion d'une association,
              réservation de studio de répétition par exemple...
        </p>
      </section>
    </div>
      
    <?php 
      require 'pages/footer.php';
    ?>

  </body>
</html>
