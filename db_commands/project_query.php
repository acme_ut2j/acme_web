<?php 
    $stat = "SELECT compositions.saison, commandes.projet, projets.titre, projets.description, commandes.client, clients.nom, clients.prenom
    FROM projets
        JOIN commandes ON commandes.projet = projets.id
        JOIN clients ON clients.id = commandes.client
        JOIN compositions on compositions.projet = projets.id
        JOIN saisons on saisons.id = compositions.saison
	WHERE saisons.num = ? ORDER BY projets.id";

    $req_p1 = $bdd->prepare($stat);
    $req_p1->execute(array($_GET['num']));
?>